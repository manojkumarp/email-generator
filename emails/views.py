from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import render_to_string


activate_context = {'data': [
    {'type':"hi", 'value': "Hey"},
    {'type':"text", 'value': ["Greetings from PrepLane!!!"]},
    {'type':"text", 'value': ['We are glad to have you on board. Your email (( email )) has been used to register on PrepLane.']},
    {'type':"text", 'value': ['To ensure that you make the most of your time, I would like to personally take you through the initial steps and help you feel comfortable using PrepLane.']},
    {'type':"text", 'value': ['Please verify your email by clicking the link below:']},
    {'type':"link", 'value': "VERIFY MY EMAIL ID"},
    {'type':"text", 'value': ["I'll be waiting for your confirmation to share the next steps with you and begin your preparation using PrepLane."]},
    {'type':"text", 'value': ["See you soon!", "Cheers,"]},
    {'type':"text", 'value': ["Nitesh / Vivek from PrepLane"]}
    ]}

def index(request):
    return render(request, 'download.html')

def active(request):
    if "download" in request.GET:
        if request.GET["download"].lower() == "true":
            content = render_to_string('body.html', activate_context)
            return HttpResponse(content, content_type='text/plain')
    return render(request, 'body.html', activate_context)

